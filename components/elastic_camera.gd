extends Camera2D

export(NodePath) var first_target_path
export(NodePath) var second_target_path
export(Vector2) var margin = Vector2(400, 400)

onready var first_target = get_node(first_target_path)
onready var second_target = get_node(second_target_path)

func compute_zoom():
	if (first_target and second_target):
		var distance = first_target.position - second_target.position
		distance.x = (abs(distance.x) + margin.x)/ get_viewport().size.x
		distance.y = (abs(distance.y) + margin.y) / get_viewport().size.y
		distance = max(distance.x, distance.y)
		
		distance = max(distance, 1.5)
		
		zoom = Vector2(distance, distance)
		print(zoom)

func compute_position():
	position = (first_target.position + second_target.position) / 2
	position -= (get_viewport().size * zoom) / 2
	#position += Vector2((1920 * zoom.x) / 8, (1080 * zoom.y) / 8) 
	
func _process(delta):
	#print(global_position)
	compute_zoom()
	compute_position()
