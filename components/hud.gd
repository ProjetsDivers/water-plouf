extends CanvasLayer


func _ready():
	pass # Replace with function body.

func p1_change_life(value):
	$container_p1/MarginContainer/life_bar.value = value / 5
	
func p2_change_life(value):
	$container_p2/MarginContainer/life_bar.value = value / 5

func p3_change_life(value):
	$container_p3/MarginContainer/life_bar.value = value / 5