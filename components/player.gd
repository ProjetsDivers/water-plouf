extends KinematicBody2D

export(float) var MAX_LIFE = 500
export(float) var damage = 50

onready var explode_particles = preload("res://particles/explosion_particle.tscn")

export(String) var input_prefix = ""
export(int) var bullet_collision_layer = 0
var _speed = 50
var _angle = 0
var _direction: Vector2 = Vector2(-1, 0)
var _direction_speed = 3
var _speed_boost = 1
var _speed_boost_increment = 8
var _max_speed_boost = 6
var _min_speed_boost = 1
var _tick = 0
var _shoot_speed = 0.5
var _life = MAX_LIFE

var _is_fireloading = false
export(float) var _fireloading_duration = 0.5
var _fireloading_tick = 0

var _is_ultrafire = false
export(float) var _ultrafire_duration = 0.15
export(float) var _ultrafire_tick_number = 3
export(float) var _ultrafire_tick = 0

var life = MAX_LIFE

signal hurted
signal destroyed

onready var _bullet = preload("res://components/bullet.tscn")

func _ready():
	pass # Replace with function body.

func get_input_prefix():
	return input_prefix


func garbage():
	queue_free()

func die():
	emit_signal("destroyed", position)
	$AnimationPlayer.play("die")
	
func hit(damage):
	life -= damage
	$AnimationPlayer.play("hit")
	emit_signal("hurted", life)
	if (life <= 0):
		die()

func shoot(delta):
	$cannon_fire_sound.play()
	for j in [-1, 1]:
		for i in range(0, 3):
			var bullet = _bullet.instance()
			bullet.global_position = $cannon.global_position + i * _direction * 15
			var angle = _angle - 1.5 * j
			bullet.set_collision_layer_bit(bullet_collision_layer, true)
			bullet.direction = Vector2(cos(angle), sin(angle))
			bullet.rotation = angle
			bullet.damage = damage
			get_node("..").add_child(bullet)


func compute_direction(delta):
	
	if Input.is_action_pressed(get_input_prefix() + "_ultrafire"):
		_is_fireloading = true
		_speed_boost = max(_min_speed_boost, _speed_boost - _speed_boost_increment * delta * 0.05)
		_fireloading_tick += delta
		if (_fireloading_tick > _fireloading_duration):
			_ultrafire_tick += delta
			if (_ultrafire_tick >= _ultrafire_duration):
				_ultrafire_tick = 0
				shoot(delta)
	else:
		_is_fireloading = false
		_fireloading_tick = 0
		
		if Input.is_action_pressed(get_input_prefix() + "_left"):
			_angle -= _direction_speed * delta
		elif Input.is_action_pressed(get_input_prefix() + "_right"):
			_angle += _direction_speed * delta
		
		if Input.is_action_pressed(get_input_prefix() + "_boost"):
			_speed_boost = min(_speed_boost + _speed_boost_increment * delta, _max_speed_boost)
		else:
			_speed_boost = max(_min_speed_boost, _speed_boost - _speed_boost_increment * delta * 0.3)
		
		if Input.is_action_pressed(get_input_prefix() + "_slow"):
			_speed_boost = max(_min_speed_boost, _speed_boost - _speed_boost_increment * delta * 2)
	
	_direction = Vector2(cos(_angle), sin(_angle))

func compute_shooting(delta):
	_tick += delta

	if Input.is_action_pressed(get_input_prefix() + "_shoot") and _tick >= _shoot_speed:
		shoot(delta)
		_tick = 0


func _process(delta):
	compute_direction(delta)
	move_and_collide(_direction * _speed * _speed_boost * delta)
	compute_shooting(delta)
	look_at(position + _direction)
	pass
