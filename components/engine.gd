extends Node2D

func _ready():
	connect_hud()

func particles_animation(position):
	print("particles_animation")
	$explosion_particle.position = position
	$explosion_particle.emitting = true
	$explosion_particle.restart()

func connect_hud():
	$game_place/player_1.connect("hurted", $hud, "p1_change_life")
	$game_place/player_2.connect("hurted", $hud, "p2_change_life")
	
	$game_place/player_1.connect("destroyed", self, "particles_animation")
	$game_place/player_2.connect("destroyed", self, "particles_animation")