extends KinematicBody2D

const BULLET_SPEED = 800
const DAMAGE = 20 

var speed = BULLET_SPEED

onready var damage = DAMAGE
var direction = Vector2(0, 0)

func garbage():
	queue_free()

func _physics_process(delta):
	var collision_info = move_and_collide(speed * delta * direction)
	if collision_info:
		var collider = collision_info.collider
		if collider.has_method("hit"):
			collider.hit(damage)
		garbage()

func set_direction(d):
	direction = d